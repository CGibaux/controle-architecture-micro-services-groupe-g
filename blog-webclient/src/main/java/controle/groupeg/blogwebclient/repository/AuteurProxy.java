package controle.groupeg.blogwebclient.repository;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import controle.groupeg.blogwebclient.model.Auteur;

@Repository
public class AuteurProxy extends ClassicProxy {

	public Auteur getAuteur(int id) {
		
		String getAuteurUrl = props.getApiUrl() + "/auteur/" + id;
		
		ResponseEntity<Auteur> response = restTemplate.exchange(
				getAuteurUrl, 
				HttpMethod.GET, 
				null, 
				Auteur.class);
		
		return response.getBody();		
	}
	
	public Iterable<Auteur> getAuteurs() {
		
		String getAuteursUrl = props.getApiUrl() + "/auteur";
		
		ResponseEntity<Iterable<Auteur>> response = restTemplate.exchange(
				getAuteursUrl, 
				HttpMethod.GET, 
				null, 
				new ParameterizedTypeReference<Iterable<Auteur>>() {});
		
		return response.getBody();
	}
	
	public Auteur createAuteur(Auteur auteur) {
		
		String createAuteursUrl = props.getApiUrl() + "/auteur";
		
		HttpEntity<Auteur> requestEntity = new HttpEntity<Auteur>(auteur);
		ResponseEntity<Auteur> response = restTemplate.exchange(
				createAuteursUrl, 
				HttpMethod.POST, 
				requestEntity, 
				Auteur.class);
		
		return response.getBody();
	}
	
	public void deleteAuteur(Integer id) {
		
		String deleteAuteursUrl = props.getApiUrl() + "/auteur/" + id;
		
		ResponseEntity<Void> response = restTemplate.exchange(
				deleteAuteursUrl, 
				HttpMethod.DELETE,
				null,
				Void.class);
		
		System.out.println(response.getStatusCode().toString());		
	}
	
	public Auteur updateAuteur(Auteur auteur) {
		
		String updateAuteursUrl = props.getApiUrl() + "/auteur/" + auteur.getId();
		
		HttpEntity<Auteur> requestEntity = new HttpEntity<Auteur>(auteur);
		ResponseEntity<Auteur> response = restTemplate.exchange(
				updateAuteursUrl, 
				HttpMethod.PUT, 
				requestEntity, 
				Auteur.class);
		
		return response.getBody();
	}

	public static Iterable<Auteur> getNom() {
		// TODO Auto-generated method stub
		return null;
	}
	
}