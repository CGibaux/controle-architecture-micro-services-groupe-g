package controle.groupeg.blogwebclient.repository;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import controle.groupeg.blogwebclient.model.Article;

@Repository
public class ArticleProxy extends ClassicProxy  {
	
public Article getArticle(int id) {
		
		String getArticleUrl = props.getApiUrl() + "/article/" + id;
		
		ResponseEntity<Article> response = restTemplate.exchange(
				getArticleUrl, 
				HttpMethod.GET, 
				null, 
				Article.class);
		
		return response.getBody();		
	}
	
	public Iterable<Article> getArticles() {
		
		String getArticlesUrl = props.getApiUrl() + "/article";
		
		ResponseEntity<Iterable<Article>> response = restTemplate.exchange(
				getArticlesUrl, 
				HttpMethod.GET, 
				null, 
				new ParameterizedTypeReference<Iterable<Article>>() {});
		
		return response.getBody();
	}
	
	public Article createArticle(Article article) {
		
		String createArticlesUrl = props.getApiUrl() + "/article";
		
		HttpEntity<Article> requestEntity = new HttpEntity<Article>(article);
		ResponseEntity<Article> response = restTemplate.exchange(
				createArticlesUrl, 
				HttpMethod.POST, 
				requestEntity, 
				Article.class);
		
		return response.getBody();
	}
	
	public void deleteArticle(Integer id) {
		
		String deleteArticlesUrl = props.getApiUrl() + "/article/" + id;
		
		ResponseEntity<Void> response = restTemplate.exchange(
				deleteArticlesUrl, 
				HttpMethod.DELETE,
				null,
				Void.class);
		
		System.out.println(response.getStatusCode().toString());		
	}
	
	public Article updateArticle(Article article) {
		
		String updateArticlesUrl = props.getApiUrl() + "/article/" + article.getId();
		
		HttpEntity<Article> requestEntity = new HttpEntity<Article>(article);
		ResponseEntity<Article> response = restTemplate.exchange(
				updateArticlesUrl, 
				HttpMethod.PUT, 
				requestEntity, 
				Article.class);
		
		return response.getBody();
	}

	public static Iterable<Article> getNom() {
		// TODO Auto-generated method stub
		return null;
	}

	public Iterable<Article> getArticleByCategorie(int id) {
		String getArticleUrl = props.getApiUrl() + "/article/category/" + id;
        ResponseEntity<Iterable<Article>> response = restTemplate.exchange(
                getArticleUrl, 
                HttpMethod.GET, 
                null, 
                new ParameterizedTypeReference<Iterable<Article>>() {});
        return response.getBody();
	}


}
