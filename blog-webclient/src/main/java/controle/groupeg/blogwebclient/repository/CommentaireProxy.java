package controle.groupeg.blogwebclient.repository;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import controle.groupeg.blogwebclient.model.Commentaire;

@Repository
public class CommentaireProxy extends ClassicProxy  {

public Commentaire getCommentaire(int id) {
		
		String getCommentaireUrl = props.getApiUrl() + "/commentaire/" + id;
		
		ResponseEntity<Commentaire> response = restTemplate.exchange(
				getCommentaireUrl, 
				HttpMethod.GET, 
				null, 
				Commentaire.class);
		
		return response.getBody();		
	}
	
	public Iterable<Commentaire> getCommentaires() {
		
		String getCommentairesUrl = props.getApiUrl() + "/commentaire";
		
		ResponseEntity<Iterable<Commentaire>> response = restTemplate.exchange(
				getCommentairesUrl, 
				HttpMethod.GET, 
				null, 
				new ParameterizedTypeReference<Iterable<Commentaire>>() {});
		
		return response.getBody();
	}
	
	public Commentaire createCommentaire(Commentaire commentaire) {
		
		String createCommentairesUrl = props.getApiUrl() + "/commentaire";
		
		HttpEntity<Commentaire> requestEntity = new HttpEntity<Commentaire>(commentaire);
		ResponseEntity<Commentaire> response = restTemplate.exchange(
				createCommentairesUrl, 
				HttpMethod.POST, 
				requestEntity, 
				Commentaire.class);
		
		return response.getBody();
	}
	
	public void deleteCommentaire(Integer id) {
		
		String deleteCommentairesUrl = props.getApiUrl() + "/commentaire/" + id;
		
		ResponseEntity<Void> response = restTemplate.exchange(
				deleteCommentairesUrl, 
				HttpMethod.DELETE,
				null,
				Void.class);
		
		System.out.println(response.getStatusCode().toString());		
	}
	
	public Commentaire updateCommentaire(Commentaire commentaire) {
		
		String updateCommentairesUrl = props.getApiUrl() + "/commentaire/" + commentaire.getId();
		
		HttpEntity<Commentaire> requestEntity = new HttpEntity<Commentaire>(commentaire);
		ResponseEntity<Commentaire> response = restTemplate.exchange(
				updateCommentairesUrl, 
				HttpMethod.PUT, 
				requestEntity, 
				Commentaire.class);
		
		return response.getBody();
	}

	public static Iterable<Commentaire> getNom() {
		// TODO Auto-generated method stub
		return null;
	}
}
