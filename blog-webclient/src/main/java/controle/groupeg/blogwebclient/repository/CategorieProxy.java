package controle.groupeg.blogwebclient.repository;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import controle.groupeg.blogwebclient.model.Categorie;

@Repository
public class CategorieProxy extends ClassicProxy  {
	
	public Categorie getCategorie(int id) {
		
		String getCategorieUrl = props.getApiUrl() + "/category/" + id;
		
		ResponseEntity<Categorie> response = restTemplate.exchange(
				getCategorieUrl, 
				HttpMethod.GET, 
				null, 
				Categorie.class);
		
		return response.getBody();		
	}
	
	public Iterable<Categorie> getCategories() {
		
		String getCategoriesUrl = props.getApiUrl() + "/category";
		
		ResponseEntity<Iterable<Categorie>> response = restTemplate.exchange(
				getCategoriesUrl, 
				HttpMethod.GET, 
				null, 
				new ParameterizedTypeReference<Iterable<Categorie>>() {});
		
		return response.getBody();
	}
	
	public Categorie createCategorie(Categorie categorie) {
		
		String createCategoriesUrl = props.getApiUrl() + "/category";
		
		HttpEntity<Categorie> requestEntity = new HttpEntity<Categorie>(categorie);
		ResponseEntity<Categorie> response = restTemplate.exchange(
				createCategoriesUrl, 
				HttpMethod.POST, 
				requestEntity, 
				Categorie.class);
		
		return response.getBody();
	}
	
	public void deleteCategorie(Integer id) {
		
		String deleteCategoriesUrl = props.getApiUrl() + "/category" + id;
		
		ResponseEntity<Void> response = restTemplate.exchange(
				deleteCategoriesUrl, 
				HttpMethod.DELETE,
				null,
				Void.class);
		
		System.out.println(response.getStatusCode().toString());		
	}
	
	public Categorie updateCategorie(Categorie categorie) {
		
		String updateCategoriesUrl = props.getApiUrl() + "/category" + categorie.getId();
		
		HttpEntity<Categorie> requestEntity = new HttpEntity<Categorie>(categorie);
		ResponseEntity<Categorie> response = restTemplate.exchange(
				updateCategoriesUrl, 
				HttpMethod.PUT, 
				requestEntity, 
				Categorie.class);
		
		return response.getBody();
	}

	public static Iterable<Categorie> getNom() {
		// TODO Auto-generated method stub
		return null;
	}

}
