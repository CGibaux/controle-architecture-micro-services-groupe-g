package controle.groupeg.blogwebclient.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import controle.groupeg.blogwebclient.config.CustomProperties;

@Repository
public class ClassicProxy {

	@Autowired
	protected RestTemplate restTemplate;
	
	@Autowired
	protected CustomProperties props;
		
}