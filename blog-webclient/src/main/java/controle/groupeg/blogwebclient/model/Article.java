package controle.groupeg.blogwebclient.model;

import java.util.Date;

public class Article {
	
	private Integer id;
    private Date datearticle;
    private String contenu;
    private Integer idauteur;
    private Integer idcategorie;
    
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Date getDatearticle() {
        return datearticle;
    }
    
    public void setDatearticle(Date datearticle) {
        this.datearticle = datearticle;
    }
    
    public String getContenu() {
        return contenu;
    }
    
    public void setContenu(String contenu) {
        this.contenu = contenu;
    }
    
    public Integer getIdauteur() {
        return idauteur;
    }
    
    public void setIdauteur(Integer idauteur) {
        this.idauteur = idauteur;
    }
    
    public Integer getIdcategorie() {
        return idcategorie;
    }
    
    public void setIdcategorie(Integer idcategorie) {
        this.idcategorie = idcategorie;
    }
	
}
