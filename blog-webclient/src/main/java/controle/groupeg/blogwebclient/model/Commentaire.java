package controle.groupeg.blogwebclient.model;

import java.util.Date;

public class Commentaire {
	
	private Integer id;
	private String contenu;
	private Date datecommentaire;
	private Integer idauteur;
	private Integer idarticle;
	
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getContenu() {
		return contenu;
	}
	
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	
	public Date getDatecommentaire() {
		return datecommentaire;
	}
	
	public void setDatecommentaire(Date datecommentaire) {
		this.datecommentaire = datecommentaire;
	}
	
	public Integer getIdauteur() {
		return idauteur;
	}
	
	public void setIdauteur(Integer idauteur) {
		this.idauteur = idauteur;
	}
	
	public Integer getIdarticle() {
		return idarticle;
	}
	
	public void setIdarticle(Integer idarticle) {
		this.idarticle = idarticle;
	}
	
}
