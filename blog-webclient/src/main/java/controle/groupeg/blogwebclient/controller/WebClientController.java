package controle.groupeg.blogwebclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import controle.groupeg.blogwebclient.model.Article;
import controle.groupeg.blogwebclient.model.Auteur;
import controle.groupeg.blogwebclient.model.Categorie;
import controle.groupeg.blogwebclient.model.Commentaire;
import controle.groupeg.blogwebclient.repository.ArticleProxy;
import controle.groupeg.blogwebclient.repository.AuteurProxy;
import controle.groupeg.blogwebclient.repository.CategorieProxy;
import controle.groupeg.blogwebclient.repository.CommentaireProxy;



@Controller
public class WebClientController {
	
	@Autowired
	ArticleProxy articleProxy;

	@Autowired
	AuteurProxy auteurProxy;
	
	@Autowired
	CommentaireProxy commentaireProxy;
	
	@Autowired
	CategorieProxy categorieProxy;
	
	@GetMapping("/")
    public String getHomePage(Model model) {
		
        Iterable<Article> articles = articleProxy.getArticles();
        model.addAttribute("articles", articles);
        
        Iterable<Auteur> auteurs = auteurProxy.getAuteurs();
        model.addAttribute("auteurs", auteurs);
        
        Iterable<Commentaire> commentaires = commentaireProxy.getCommentaires();
        model.addAttribute("commentaires", commentaires);

        return "index";
    }
	
	
	@GetMapping("/category")
    public String getCategorie(Model model) {
		
        Iterable<Categorie> categories = categorieProxy.getCategories();
        model.addAttribute("categories", categories);
        return "categorie";
    }
	
	@GetMapping(value = {"/article/{id}"})
	public String getArticleById(
			@PathVariable(name = "id", required = false) Integer id, 
			Model model) {		
		Article articles = articleProxy.getArticle(id);
		model.addAttribute("articles", articles);		
		
		int idToGet = 1;
		if(id != null) {
			idToGet = id;
		}
		Article article = articleProxy.getArticle(idToGet);
		model.addAttribute("product", article);
		
		return "article";
	}
	
	@GetMapping("/category/{id}")
	public String getArticleByCategorie(@PathVariable int id, Model model) {
		Iterable<Article> articles = articleProxy.getArticleByCategorie(id);
		model.addAttribute("articles", articles);
		return "articleCategory";
	}
	
	
	@GetMapping("/article/createarticle")
    public String getCreateArticle(Model model) {
		Article articles = new Article();
        model.addAttribute("articles", articles);
        return "addArticle";
    }
	
	@PostMapping("/article/saveArticles")
	public ModelAndView saveArticle(@ModelAttribute Article article) {
		if(article.getId() == null) {
			articleProxy.createArticle(article);
		} else {
			articleProxy.updateArticle(article);
		}
		return new ModelAndView("redirect:/");
	}
	
	
	@GetMapping("/auteur/createauteur")
    public String getCreateAuteur(Model model) {
		Auteur auteurs = new Auteur();
        model.addAttribute("auteurs", auteurs);
        return "creatAuteur";
    }
	
	@PostMapping("/auteur/saveAuteurs")
	public ModelAndView saveAuteur(@ModelAttribute Auteur auteur) {
		if(auteur.getId() == null) {
			auteurProxy.createAuteur(auteur);
		} else {
			auteurProxy.updateAuteur(auteur);
		}
		return new ModelAndView("redirect:/");
	}
	

	@GetMapping("/article/createcategory")
    public String getCreateCategory(Model model) {
		Categorie categories = new Categorie();
        model.addAttribute("categories", categories);
        return "createCategorie";
    }
	
	@PostMapping("/article/saveCategory")
	public ModelAndView saveCategory(@ModelAttribute Categorie categorie) {
		if(categorie.getId() == null) {
			categorieProxy.createCategorie(categorie);
		} else {
			categorieProxy.updateCategorie(categorie);
		}
		return new ModelAndView("redirect:/");
	}
	
	@GetMapping("/commentaire/createcommentaire")
    public String getCreateCommentaire(Model model) {
		Commentaire commentaires = new Commentaire();
        model.addAttribute("commentaires", commentaires);
        return "createCommentaire";
    }
	
	@PostMapping("/commentaire/saveCommentaires")
	public ModelAndView saveCommentaires(@ModelAttribute Commentaire commentaire) {
		if(commentaire.getId() == null) {
			commentaireProxy.createCommentaire(commentaire);
		} else {
			commentaireProxy.updateCommentaire(commentaire);
		}
		return new ModelAndView("redirect:/");
	}
	
}
