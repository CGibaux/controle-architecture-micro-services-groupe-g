package controle.groupeg.bloggateway;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {
	
	@Bean
	public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {

		return builder.routes()
				.route(r -> r.path("/article/**")
						.uri("lb://articleApi"))
				.route(r -> r.path("/auteur/**")
						.uri("lb://auteurApi"))
				.route(r -> r.path("/commentaire/**")
						.uri("lb://commentaireApi"))
				.route(r -> r.path("/category/**")
						.uri("lb://articleApi"))
				.build();
	}
	
}
