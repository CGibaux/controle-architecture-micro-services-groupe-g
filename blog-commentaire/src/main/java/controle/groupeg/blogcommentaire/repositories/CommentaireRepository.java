package controle.groupeg.blogcommentaire.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import controle.groupeg.blogcommentaire.models.Commentaire;

@Repository
public interface CommentaireRepository extends CrudRepository<Commentaire, Integer>{

}
