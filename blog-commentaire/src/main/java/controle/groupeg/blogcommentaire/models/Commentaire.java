package controle.groupeg.blogcommentaire.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="commentaires")
public class Commentaire {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="contenu")
	private String contenu;
	
	@Column(name="datecommentaire")
	private Date datecommentaire;
	
	@Column(name="idauteur")
	private Integer idauteur;
	
	@Column(name="idarticle")
	private Integer idarticle;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Date getDatecommentaire() {
		return datecommentaire;
	}

	public void setDatecommentaire(Date datecommentaire) {
		this.datecommentaire = datecommentaire;
	}

	public Integer getIdauteur() {
		return idauteur;
	}

	public void setIdauteur(Integer idauteur) {
		this.idauteur = idauteur;
	}

	public Integer getIdarticle() {
		return idarticle;
	}

	public void setIdarticle(Integer idarticle) {
		this.idarticle = idarticle;
	}
	
}
