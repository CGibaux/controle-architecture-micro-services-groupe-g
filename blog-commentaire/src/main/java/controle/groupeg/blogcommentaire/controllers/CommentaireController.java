package controle.groupeg.blogcommentaire.controllers;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import controle.groupeg.blogcommentaire.models.Commentaire;
import controle.groupeg.blogcommentaire.repositories.CommentaireRepository;

@RestController
public class CommentaireController {

	@Autowired
	private CommentaireRepository commentaireRespository;
	
	@GetMapping("/commentaire")
	public Iterable<Commentaire> getCommentaires(){
		Iterable<Commentaire> commentaires = commentaireRespository.findAll();
		System.out.println("trace getCommentaires");
		return commentaires;
	}
	
	@GetMapping("/commentaire/{id}")
	public Optional<Commentaire> getCommentaire(@PathVariable("id") int id) throws InterruptedException {
		Optional<Commentaire> commentaire = commentaireRespository.findById(id);
		if(id == 1) {
			Thread.sleep(2000);
		}
		System.out.println("Retrieve" + commentaire.get().getDatecommentaire());
		return commentaire;
	}
	
	@PostMapping("/commentaire")
	public Commentaire createCommentaire(@RequestBody Commentaire commentaire) {
		commentaire = commentaireRespository.save(commentaire);
		return commentaire;
	}
	
	@PutMapping("/commentaire/{id}")
	public Commentaire updateCommentaire(@PathVariable("id") int id, @RequestBody Commentaire commentaire) {
		Commentaire currentCommentaire = commentaireRespository.findById(id).get();
		
		String contenu = commentaire.getContenu();
		if(contenu != null) {
			currentCommentaire.setContenu(contenu);
		}
		
		Date datecommentaire = commentaire.getDatecommentaire();
		if(datecommentaire != null) {
			currentCommentaire.setDatecommentaire(datecommentaire);
		}
		
		Integer idauteur = commentaire.getIdauteur();
		if(idauteur != null) {
			currentCommentaire.setIdauteur(idauteur);
		}
		
		Integer idarticle = commentaire.getIdarticle();
		if(idarticle != null) {
			currentCommentaire.setIdarticle(idarticle);
		}
		
		currentCommentaire = commentaireRespository.save(currentCommentaire);
		return currentCommentaire;
	}
	
	@DeleteMapping("/commentaire/{id}")
	public void deleteCommentaire(@PathVariable("id") int id) {
		commentaireRespository.deleteById(id);
	}
}
