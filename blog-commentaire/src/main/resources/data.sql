DROP TABLE IF EXISTS commentaires;
 
CREATE TABLE commentaires (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  contenu VARCHAR(500),
  datecommentaire DATE(255),
  idauteur INT NOT NULL,
  idarticle INT NOT NULL
);
 
INSERT INTO commentaires (contenu, datecommentaire, idauteur, idarticle) VALUES
  ('Je suis un commentaire', '2020-12-5', 1, 1),
  ('Je suis pas drole', '2020-12-10', 2, 1),
  ('Je suis très triste', '2020-12-20', 3, 2)