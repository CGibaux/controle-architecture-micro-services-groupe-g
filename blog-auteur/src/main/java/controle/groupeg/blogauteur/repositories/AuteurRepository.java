package controle.groupeg.blogauteur.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import controle.groupeg.blogauteur.models.Auteur;

@Repository
public interface AuteurRepository  
    extends CrudRepository<Auteur, Integer> {

}
