package controle.groupeg.blogauteur.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="commentaire")
public class Commentaire {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;

	@Column(name="datecommentaire")
	private Date datecommentaire;
	
	@Column(name="idauteur")
	private Integer idauteur;
	
	@Column(name="idarticle")
	private int idarticle;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Date getdatecommentaire() {
		return datecommentaire;
	}
	
	public void setdatecommentaire(Date datecommentaire) {
		this.datecommentaire = datecommentaire;
	}
	
	public Integer getIdAuteur() {
		return idauteur;
	}
	
	public void setIdAuteur(Integer idauteur) {
		this.idauteur = idauteur;
	}
	
	public int getIdarticle() {
		return idarticle;
	}

	public void setIdarticle(int idarticle) {
		this.idarticle = idarticle;
	}
	
}
