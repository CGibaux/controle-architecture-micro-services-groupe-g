package controle.groupeg.blogauteur.models;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="article")
public class Article {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;

	@Column(name="datearticle")
	private Date datearticle;
	
	@Column(name="contenu")
	private String contenu;
	
	@Column(name="id_categorie")
	private Integer id_categorie;
	
	@Column(name="id_auteur")
	private Integer id_auteur;
	
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Date getDatearticle() {
		return datearticle;
	}
	
	public void setDatearticle(Date datearticle) {
		this.datearticle = datearticle;
	}
	
	public Integer getIdCategorie() {
		return id_categorie;
	}
	
	public void setIdCategorie(Integer id_categorie) {
		this.id_categorie = id_categorie;
	}
	
	public Integer getIdAuteur() {
		return id_auteur;
	}
	
	public void setIdAuteur(Integer id_auteur) {
		this.id_auteur = id_auteur;
	}
	
	
	
}

