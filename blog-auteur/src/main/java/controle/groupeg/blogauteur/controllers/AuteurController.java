package controle.groupeg.blogauteur.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import controle.groupeg.blogauteur.models.Auteur;
import controle.groupeg.blogauteur.repositories.AuteurRepository;

@RestController
public class AuteurController {

	@Autowired
	private AuteurRepository auteurRepository;
	    
	
	// CRUD AUTEUR
	@GetMapping("/auteur")
	public Iterable<Auteur> getAuteurs() {        
	    Iterable<Auteur> auteurs = auteurRepository.findAll();    
	    System.out.println("trace getAuteurs");
	    return auteurs;        
	}
	
	
	@GetMapping("/auteur/{id}")
	public Optional<Auteur> getAuteur(@PathVariable("id") int id) 
			throws InterruptedException {
	    Optional<Auteur> auteur = auteurRepository.findById(id);    
	    if(id == 1) {
	        Thread.sleep(2000);
	    }
	    System.out.println("Retrieve " + auteur.get().getNom());
	    return auteur;        
	}
	
	
	@PostMapping("/auteur")
	public Auteur createAuteur(@RequestBody Auteur auteur) {
	    auteur = auteurRepository.save(auteur);        
	    return auteur;
	}
	
	
	@PutMapping("/auteur/{id}")
	public Auteur updateAuteur(@PathVariable("id") int id, @RequestBody Auteur auteur) {
	    Auteur currentAuteur = auteurRepository.findById(id).get();    
	        
	    String nom = auteur.getNom();
	    if(nom != null) {
	        currentAuteur.setNom(nom);
	    }        
	    String motdepasse = auteur.getMotDePasse();
	    if(motdepasse != null) {
	        currentAuteur.setMotDePasse(motdepasse);
	    }
	    String email = auteur.getEmail();
	    if(email != null) {
	        currentAuteur.setMotDePasse(email);
	    }
	        
	    currentAuteur = auteurRepository.save(currentAuteur);
	    return currentAuteur;
	}    
	
	
	@DeleteMapping("/auteur/{id}")
	public void deleteAuteur(@PathVariable("id") int id) {
	    auteurRepository.deleteById(id);
	}
	
	
	
	
	// CONNECTION USER
	@PutMapping("/auteur/auth/{id}")
	public Auteur authAuteur(@PathVariable("id") int id, @RequestBody Auteur auteur) {
	    Auteur currentAuteur = auteurRepository.findById(id).get();
	    //String nom = auteur.getNom();    
	    //String motdepasse = auteur.getMotDePasse();
	    //Boolean connecte =  auteur.getConnecte();
	    Boolean test = currentAuteur.getConnecte();
	    
	    
	    System.out.println("CURRENT AUTEUR : " + currentAuteur.getNom() + "\n");
	    System.out.println("CE QUE JE MARQUE : " + auteur.getNom() + "\n");
	    System.out.println("L'ETAT DU BOOLEAN : " + currentAuteur.getConnecte() + "\n");
	    
	    
	   
	    
	    if(currentAuteur.getNom() != null && currentAuteur.getMotDePasse() != null) {
	    	System.out.println("IF IT WORKS");
	    	test = true;
	    }
	    else {
	    	System.out.println("TU ES DANS LE ELSE MON AMI MOUAHAHAHAHA");
	    }
	        
	    currentAuteur = auteurRepository.save(currentAuteur);
	    return currentAuteur;
	}  
	
	
}
