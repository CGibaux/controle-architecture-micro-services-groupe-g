DROP TABLE IF EXISTS auteurs;
 
CREATE TABLE auteurs (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nom VARCHAR(255),
  motdepasse VARCHAR(255),
  email VARCHAR(255),
  connecte BOOLEAN
);
 
INSERT INTO auteurs (nom, motdepasse, email, connecte) VALUES
  ('Laurent', 'laurent', 'laurent.laurent@gmail.com', false),
  ('Sophie', 'sophie', 'sophie.sophie@gmail.com', false),
  ('Agathe', 'agathe', 'agathe.agathe@gmail.com', false);