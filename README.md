# Settings Spring Initializer

## Group : controle.groupe-g
## Artifact and name : nom de la ressource
## Description : la description du projet
## Packaging: Jar
## Java: 8

# Micro-services : 
## Répartion des tâches
### Axel :
- [x] Auteur
- [x] Gateway
### Bruno :
- [x] Article (catégorie + contenu)
- [x] Eureka-server
- [x] WebClient
### Costin :
- [x] BootAdmin
- [x] Commentaire



# Consigne pour le projet
## Créer un blog en Spring Boot Java

## Un visiteur (non connecté) doit pouvoir:(10pts)
- Visualiser la liste des catégories
- Visualiser la liste des articles d’une catégorie
- Visualiser un article de façon unitaire
- Se connecter

## Un utilisateur connecté doit pouvoir:(6pts)
- Faire les actions du visiteur
- Ajouter une catégorie
- Ajouter un article
- Ajouter un commentaire


### Ports des micro-services
- API : 8080
- WEB : 8081
- EUREKA : 8082
- COMMENTAIRE : 8083
- Article : 8084
- BootAdmin : 8085
- Gateway : 8086

