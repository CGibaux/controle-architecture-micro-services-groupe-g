package controle.groupeg.blogarticle.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import controle.groupeg.blogarticle.models.Categorie;
import controle.groupeg.blogarticle.repositories.CategorieRepository;

@RestController
public class CategorieController {

	@Autowired
	private CategorieRepository categorieRespository;
	
	@GetMapping("/category")
	public Iterable<Categorie> getCategories(){
		Iterable<Categorie> categories = categorieRespository.findAll();
		System.out.println("trace getcategories");
		return categories;
	}
	
	@GetMapping("/category/{id}")
	public Optional<Categorie> getCategorie(@PathVariable("id") int id) throws InterruptedException {
		Optional<Categorie> categorie = categorieRespository.findById(id);
		if(id == 1) {
			Thread.sleep(2000);
		}
		System.out.println("Retrieve" + categorie.get().getNom());
		return categorie;
	}
	
	@PostMapping("/category")
	public Categorie createCategorie(@RequestBody Categorie categorie) {
		categorie = categorieRespository.save(categorie);
		return categorie;
	}
	
	@PutMapping("/category/{id}")
	public Categorie updateCategorie(@PathVariable("id") int id, @RequestBody Categorie categorie) {
		Categorie currentcategorie = categorieRespository.findById(id).get();
		
		String nom = categorie.getNom();
		if(nom != null) {
			currentcategorie.setNom(nom);
		}
		
		currentcategorie = categorieRespository.save(currentcategorie);
		return currentcategorie;
	}
	
	@DeleteMapping("/category/{id}")
	public void deleteCategorie(@PathVariable("id") int id) {
		categorieRespository.deleteById(id);
	}
}
