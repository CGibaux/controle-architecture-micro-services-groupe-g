package controle.groupeg.blogarticle.controllers;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import controle.groupeg.blogarticle.models.Article;
import controle.groupeg.blogarticle.repositories.ArticleRepository;

@RestController
public class ArticleController {

	@Autowired
	private ArticleRepository articleRespository;
	
	@GetMapping("/article")
	public Iterable<Article> getArticles(){
		Iterable<Article> articles = articleRespository.findAll();
		System.out.println("trace getarticles");
		return articles;
	}
	
	@GetMapping("/article/{id}")
	public Optional<Article> getArticle(@PathVariable("id") int id) {
		Optional<Article> article = articleRespository.findById(id);
		System.out.println("Retrieve" + article.get().getDatearticle());
		return article;
	}
	
	@PostMapping("/article")
	public Article createArticle(@RequestBody Article article) {
		article = articleRespository.save(article);
		return article;
	}
	
	@PutMapping("/article/{id}")
	public Article updateArticle(@PathVariable("id") int id, @RequestBody Article article) {
		Article currentarticle = articleRespository.findById(id).get();
		
		Date datearticle = article.getDatearticle();
		if(datearticle != null) {
			currentarticle.setDatearticle(datearticle);
		}
		
		String contenu = article.getContenu();
		if(contenu != null) {
			currentarticle.setContenu(contenu);
		}
		
		Integer idauteur = article.getIdauteur();
		if(idauteur != null) {
			currentarticle.setIdauteur(idauteur);
		}	
		currentarticle = articleRespository.save(currentarticle);
		return currentarticle;
	}
	
	@DeleteMapping("/article/{id}")
	public void deleteArticle(@PathVariable("id") int id) {
		articleRespository.deleteById(id);
	}
	
	
	@GetMapping("/article/category/{id}")
	public Iterable<Article> getArticleByCategorie(@PathVariable int id, Model model) {
		Iterable<Article> articles = articleRespository.findaAllByCategoryId(id);
		return articles;
	}
	
}
