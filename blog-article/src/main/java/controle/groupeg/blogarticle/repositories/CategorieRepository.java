package controle.groupeg.blogarticle.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import controle.groupeg.blogarticle.models.Categorie;

@Repository
public interface CategorieRepository extends CrudRepository<Categorie, Integer>{

}

