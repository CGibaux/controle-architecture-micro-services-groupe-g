package controle.groupeg.blogarticle.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;


@Entity
@Table(name="articles")
public class Article {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;

	@Column(name="datearticle")
	@CreationTimestamp
	private Date datearticle;
	
	@Column(name="contenu")
	private String contenu;
	
	@Column(name="idcategorie")
	private Integer idcategorie;
	
	@Column(name="idauteur")
	private Integer idauteur;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDatearticle() {
		return datearticle;
	}
	public void setDatearticle(Date datearticle) {
		this.datearticle = datearticle;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public Integer getIdauteur() {
		return idauteur;
	}
	public void setIdauteur(Integer idauteur) {
		this.idauteur = idauteur;
	}
	public Integer getIdcategorie() {
		return idcategorie;
	}
	public void setIdcategorie(Integer idcategorie) {
		this.idcategorie = idcategorie;
	}
}
